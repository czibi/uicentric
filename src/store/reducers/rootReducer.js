const initialState={
    todos: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case "LOAD_DATA_FROM_API":
            return {
                ...state, todos: action.data
            }
        case "CURRENT_TODO":
            const index= !isNaN(Number(action.id)) && Number(action.id)
            return {
                ...state, currentTodo: state.todos.find(todo => (todo.id === index))
            }
        default:
            return state;
    } 
};