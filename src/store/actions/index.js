export const API = () => {
    return (dispatch) => {
        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response =>{
                if (!response.ok) { throw response }
                return response.json();
            })
            .then(data => {
                dispatch({
                    type: "LOAD_DATA_FROM_API",
                    data: data,
                })
            }).catch(error => console.error('Errors: ', error));
    }
}

export const currentTodo = id => ({
    type: 'CURRENT_TODO',
    id,
})
