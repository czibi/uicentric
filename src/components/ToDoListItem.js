import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Typography, Card, Chip, Button, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { common, blue } from '@material-ui/core/colors';
import clsx from 'clsx';

const useStyles = makeStyles({
    containerFiller: {
        padding: "15px 10px",
        height: "100%",
    },
    cardFiller: {
        height: "100%",
    },
    cardTitle: {
        marginBottom: "20px",
    },
    chip: {
        color: common.white,
        fontWeight: "600",
        marginBottom: "10px",
    },
    completed: {
        backgroundColor: blue[300],

    },
    addMarginTop: {
        marginTop: "5px",
    },
});

const ToDoListItem = ({ todo }) => {
    const classes = useStyles();
    return (
        <Card className={classes.cardFiller}>
            <Grid container direction="column" className={classes.containerFiller}>
                <Grid item>
                    <Typography
                        className={classes.cardTitle}
                        variant="subtitle2"
                        gutterBottom
                    >
                        {todo.title}
                    </Typography>
                </Grid>
                <Grid item xs></Grid>
                <Grid item>
                    <Chip
                        className={todo.completed ? clsx(classes.chip, classes.completed) : classes.chip}
                        label={todo.completed ? "Completed" : "Not Completed"}
                    />
                </Grid>
                <Grid item>
                    <Divider className={classes.addMarginTop} />
                    <Button
                        component={Link}
                        to={`/todo/${todo.id}`}
                        variant="text"
                        color="primary"
                    >
                        View Details
                    </Button>
                </Grid>
            </Grid>
        </Card>
    )
}

export default ToDoListItem;
