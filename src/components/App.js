import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import SingleToDo from '../containers/SingleToDo';
import ToDoList from '../containers/ToDoList';

class App extends React.Component {
  render() {
    return (
      <>
          <Router>
            <Route exact path="/" component={ToDoList} />
            <Route exact path="/todo/:todoId" component={SingleToDo} />
          </Router>
      </>
    )
  }
}

export default App;
