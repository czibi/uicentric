import React from 'react';
import ToDoListItem from '../components/ToDoListItem';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from '../store/actions/index';
import {Grid, Typography} from '@material-ui/core';

class ToDoList extends React.Component {
  componentDidMount(){
    this.props.API();
  }
  render(){
    return (
      <Grid container spacing={10}>
        <Grid item xs={12}>
          <Typography variant="h2" align="center">To-do List</Typography>
        </Grid>
        <Grid item>
          <Grid container spacing={2}>
            {this.props.todos.map((todo, index) => (
              <Grid key={index} item xs={6} md={3} lg={2}>
                <ToDoListItem todo={todo}/>
              </Grid>
            ))}
          </Grid>
          </Grid>
      </Grid>
    )
  }
}

function mapStateToProps(state){
  return {
    todos: state.todos
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(ToDoList)
