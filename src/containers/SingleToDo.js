import React from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from '../store/actions/index';
import { Link } from 'react-router-dom';
import { Grid, Typography, Card, Chip, Button, Avatar, Divider } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { pink, common, blue } from '@material-ui/core/colors';
import PersonIcon from '@material-ui/icons/Person';
import AssignmentIcon from '@material-ui/icons/Assignment';
import clsx from 'clsx';

const styles = {
    containerPadding: {
        padding: "15px 10px",
    },
    cardTitle: {
        marginBottom: "20px",
    },
    chip: {
        color: common.white,
        fontWeight: "600",
        margin: "10px 0px",
    },
    completed: {
        backgroundColor: blue[300],
    },
    avatar: {
        margin: "10px auto",
        color: common.white,
        backgroundColor: pink[500],
    },
    iconContainer: {
        textAlign: "center",
    },
    addMarginBottom: {
        marginBottom: "10px",
    },
    addMarginTop: {
        marginTop: "80px",
    },
};

class SingleToDo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            todos: [],
            currentTodo: null,
        }
    }
    
    static getDerivedStateFromProps(props, state){
        if(props.todos.length !== 0){
            return  props.currentTodo(props.match.params.todoId);
        }
        props.API();
        return state;
    }
    
    render(){
        if(!this.props.todo){
            return <p>Loading..</p>;
        }
        return (
            <Card className={this.props.classes.addMarginTop}>
                <Grid container direction="column" className={this.props.classes.containerPadding}>
                    <Grid item>
                        <Typography
                            className={this.props.classes.cardTitle}
                            variant="h4"
                            gutterBottom
                        >
                            {this.props.todo.title}
                        </Typography>
                        <Divider className={this.props.classes.addMarginBottom}/>
                    </Grid>
                    <Grid item>
                        <Typography variant="body1">Additional Information:</Typography>
                        <Grid container spacing={4} direction="row" justify="center" >
                            <Grid item className={this.props.classes.iconContainer}>
                                <Avatar className={this.props.classes.avatar}>
                                    <PersonIcon />
                                </Avatar>
                                <Typography variant="h6" display="inline">User ID: {this.props.todo.userId}</Typography>
                            </Grid>
                            <Grid item className={this.props.classes.iconContainer}>
                                <Avatar className={this.props.classes.avatar}>
                                <AssignmentIcon />
                                </Avatar>
                                <Typography variant="h6" display="inline">To-do ID: {this.props.todo.id}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs>
                        <Divider className={this.props.classes.addMarginBottom}/>
                        <Typography variant="body1">To-do status:</Typography>
                    </Grid>
                    <Grid item>
                        <Chip
                            className={this.props.todo.completed ? clsx(this.props.classes.chip, this.props.classes.completed) : this.props.classes.chip}
                            label={this.props.todo.completed ? "Completed" : "Not Completed"}
                        />
                    </Grid>
                    <Grid item>
                        <Divider className={this.props.classes.addMarginBottom}/>
                        <Button
                            component={Link}
                            to={`/`}
                            variant="text"
                            color="primary"
                        >
                            Go Back
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        )
    }
}

function mapStateToProps(state){
    return {
      todo: state.currentTodo,
      todos: state.todos,
    }
}
  
function mapDispatchToProps(dispatch){
    return bindActionCreators(actions,dispatch);
}
  
export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(SingleToDo));
